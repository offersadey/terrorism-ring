
// Create TATP components
CREATE (OxygenatedWater:Component {comp_name:'Oxygenated water'})
CREATE (Acetone:Component {comp_name:'Acetone'})
CREATE (HydrochloricAcid:Component {comp_name:'Hydrochloric acid'})
CREATE (SulfuricAcid:Component {comp_name:'Sulfuric acid'})
CREATE (NitricAcid:Component {comp_name:'Nitric acid'})


// Add sample data : Phone buyers (Burners) - based on (Fake) ID Card
CREATE (GuillaumeMalotruDebailly:Person {first_name:'Guillaume Malotru', last_name:'Debailly', address:'125 Rue de Rivoli', city:'PARIS', country:'FRANCE', phone:'+33666666601'})
CREATE (HenriDuflot:Person {first_name:'Henri', last_name:'Duflot', address:'16 Rue du 4 Septembre', city:'PARIS', country:'FRANCE', phone:'+33666666602'})
CREATE (DocteurBalmes:Person {first_name:'Docteur', last_name:'Balmes', address:'44 Rue de Turbigo', city:'PARIS', country:'FRANCE', phone:'+33666666603'})
CREATE (MarinaLoiseau:Person {first_name:'Marina', last_name:'Loiseau', address:'53 Rue du Figuier', city:'PARIS', country:'FRANCE', phone:'+33666666604'})
CREATE (NadiaElMansour:Person {first_name:'Nadia', last_name:'El Mansour', address:'4 Rue Gracieuse', city:'PARIS', country:'FRANCE', phone:'+33666666605'})
CREATE (MarcLaure:Person {first_name:'Marc', last_name:'Laure', address:'56 Rue de Vaugirard', city:'PARIS', country:'FRANCE', phone:'+33666666606'})
CREATE (RachidBenarfa:Person {first_name:'Rachid', last_name:'Benarfa', address:'4 Rue Bixio', city:'PARIS', country:'FRANCE', phone:'+33666666607'})
CREATE (RaymondSisteron:Person {first_name:'Raymond', last_name:'Sisteron', address:'73 Rue Montalivet', city:'PARIS', country:'FRANCE', phone:'+33666666608'})
CREATE (CelineDelorme:Person {first_name:'C�line', last_name:'Delorme', address:'6 Rue Roqu�pine', city:'PARIS', country:'FRANCE', phone:'+33666666609'})
CREATE (PepeJeunot:Person {first_name:'P�p�', last_name:'Jeunot', address:'10 Rue de Sur�ne', city:'PARIS', country:'FRANCE', phone:'+33666666610'})
CREATE (MemeViolente:Person {first_name:'M�m�', last_name:'Violente', address:'4 Rue de Duras', city:'PARIS', country:'FRANCE', phone:'+33666666611'})
CREATE (NadimZahar:Person {first_name:'Nadim', last_name:'Zahar', address:'58 Rue des Saussaies', city:'PARIS', country:'FRANCE', phone:'+33666666612'})
CREATE (PruneDebailly:Person {first_name:'Prune', last_name:'Debailly', address:'77 Rue Saint Honor�', city:'PARIS', country:'FRANCE', phone:'+33666666613'})
CREATE (SylvainEllenstein:Person {first_name:'Sylvain', last_name:'Ellenstein', address:'8 Rue Duphot', city:'PARIS', country:'FRANCE', phone:'+33666666614'})
CREATE (HachemAlKhatib:Person {first_name:'Hachem', last_name:'AlKhatib', address:'4 Place Vendome', city:'PARIS', country:'FRANCE', phone:'+33666666615'})
CREATE (JeromeLebrun:Person {first_name:'J�r�me', last_name:'Lebrun', address:'5 Rue Danielle Casanova', city:'PARIS', country:'FRANCE', phone:'+33666666616'})
CREATE (GaingouinRiou:Person {first_name:'Gaingouin', last_name:'Riou', address:'66 Rue Louis Le Grand', city:'PARIS', country:'FRANCE', phone:'+33666666617'})
CREATE (TristanSinzou:Person {first_name:'Tristan', last_name:'Sinzou', address:'6 Rue Saint Roch', city:'PARIS', country:'FRANCE', phone:'+33666666618'})
CREATE (SimonVidal:Person {first_name:'Simon', last_name:'Vidal', address:'5 Rue Th�r�se', city:'PARIS', country:'FRANCE', phone:'+33666666619'})
CREATE (GherbiRheda:Person {first_name:'Gherbi', last_name:'Rheda', address:'8 Rue des Pyramides', city:'PARIS', country:'FRANCE', phone:'+33666666620'})



// Stolen Detonators
CREATE (DetonatorD25:Detonator {type:'D25-Type01', address:'65 Boulevard des Italiens', city:'PARIS', country:'FRANCE', stolen_date:'2017/05/03 15:28:00'})

// Create relationships between Shop/pharmacy Buyers (Fake ID Card) and Components
CREATE (GuillaumeMalotruDebailly)-[:BOUGHT {date:'2017/05/02 10:28:00', shop:'Pharmacie Rovoli', shop_address:'33 Rue de Rivoli', shop_city:'PARIS', shop_country:'FRANCE', qty:'20'}]->(OxygenatedWater)
CREATE (GuillaumeMalotruDebailly)-[:BOUGHT {date:'2017/05/03 11:28:00', shop:'Pharmacie Arts et M�tiers', shop_address:'156 Rue de Turbigo', shop_city:'PARIS', shop_country:'FRANCE', qty:'40'}]->(Acetone)
CREATE (HenriDuflot)-[:BOUGHT {date:'2017/05/04 13:10:00', shop:'Droguerie Montalivet', shop_address:'67 Rue de Montalivet', shop_city:'PARIS', shop_country:'FRANCE', qty:'60'}]->(HydrochloricAcid)
CREATE (MarcLaure)-[:BOUGHT {date:'2017/05/06 16:10:00', shop:'Droguerie Vaugirard', shop_address:'59 Rue de Vaugirard', shop_city:'PARIS', shop_country:'FRANCE', qty:'50'}]->(NitricAcid)
