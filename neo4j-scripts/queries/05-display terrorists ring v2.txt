// Terrorists Ring with possible Stolen Detonators
MATCH (person2:Person)-[:CALLED]-(person1:Person)-[*1..3]->(comp1:Component)
OPTIONAL MATCH (detonator1:Detonator)
WHERE detonator1.city = person1.city OR detonator1.city = person2.city
RETURN DISTINCT person1, person2, comp1, detonator1
