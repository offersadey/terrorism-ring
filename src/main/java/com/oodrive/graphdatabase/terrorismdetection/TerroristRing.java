package com.oodrive.graphdatabase.terrorismdetection;

import static org.neo4j.driver.v1.Values.parameters;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Config;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

public class TerroristRing {

    public static void main(String...args) {

        Config noSSL = Config.build().withEncryptionLevel(Config.EncryptionLevel.NONE).toConfig();
        Driver driver = GraphDatabase.driver("bolt://localhost",AuthTokens.basic("neo4j","neo4j"),noSSL); // User and password
        try (Session session = driver.session()) {


            StatementResult result;
            
            // Terrorists Ring with possible Stollen triggers
            String terroristQuery =  
            	" MATCH (person2:Person)-[:CALLED]-(person1:Person)-[*1..3]->(comp1:Component) " +
            	" OPTIONAL MATCH (detonator1:Detonator) " +
            	" WHERE detonator1.city = person1.city OR detonator1.city = person2.city " +
            	" RETURN DISTINCT person1, person2, comp1, detonator1";

            result = session.run(terroristQuery, parameters());
            while (result.hasNext()) {
            	Record record = result.next();
            	System.out.println("Person 1: " + record.get("person1").asMap());
            	System.out.println(" Person 2: " + record.get("person2").asMap());
            	System.out.println("  Component: " + record.get("comp1").asMap());
            	if (!record.get("detonator1").isNull()) {
            		System.out.println("   Detonator: " + record.get("detonator1").asMap());
            	}	
            }
      

        }
    }
}
